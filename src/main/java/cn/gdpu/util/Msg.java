package cn.gdpu.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Msg {
    public static Object empty(){
        Map<String,Object> map = new TreeMap<>();
        map.put("code", 0);
        map.put("count", 0);
        map.put("data", new ArrayList());
        map.put("msg", "搜索结果为空");
        return JSON.toJSON(map);
    }

    public static Object ok(){
        JSONObject json = new JSONObject();
        json.put("msg", "ok");
        return JSON.toJSON(json);
    }

    public static Object fail(){
        JSONObject json = new JSONObject();
        json.put("msg", "fail");
        return JSON.toJSON(json);
    }

    public static Object msg(String type, Object msg){
        Map<String,Object> result = new HashMap<>();
        result.put(type, msg);
        return JSON.toJSON(result);
    }

    public static Object msg(Integer code,String type, Object msg){
        Map<String,Object> result = new HashMap<>();
        result.put(type, msg);
        return JSON.toJSON(result);
    }

    //未登录时返回code -1
    public static void unlogin(HttpServletResponse response){
        try {
            Map<String,Integer> result = new HashMap<>();
            result.put("code", -1);
            String jsonStr = JSON.toJSONString(result);
            response.setContentType("application/json; charset=utf-8");
            response.getWriter().print(jsonStr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Object msg(Object msg){
        JSONObject json = new JSONObject();
        json.put("msg", msg);
        return JSON.toJSON(json);
    }

}
